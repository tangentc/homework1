﻿using System;
namespace Homework1
{
	public class Rectangle : Polygon
    {
        public double length, width;
        public Rectangle( Point point1, Point point2, Point point3, Point point4)
            :base(point1, point2, point3, point4)
        {
        }
        public override double Area()
        {
            return sides[0] * sides[1];
        }
		public override double Perimeter()
        {
            return 2 * (sides[0] + sides[1]);
        }
    }
}
