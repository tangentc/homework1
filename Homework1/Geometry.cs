﻿using System;
namespace Homework1
    {
        public class Geometry
        {
        public static double Distance(Point point1, Point point2)
            {
                return Math.Sqrt(Math.Pow((point2.y - point1.y), 2) + Math.Pow((point2.x - point1.x), 2));
            }

            

            public static double Angle(Point start, Point corner, Point end)
            {
                double linesc = Distance(start, corner);
                double linece = Distance(corner, end);
                double linese = Distance(start, end);

                return AngleByCosLaw(linesc, linece, linese);
            }
            public static double AngleByCosLaw(double LineStartToCorner,double LineCornerToEnd,double LineStartToEnd)
            {
                return Math.Acos((Math.Pow(LineStartToCorner, 2) + Math.Pow(LineCornerToEnd, 2) - Math.Pow(LineStartToEnd, 2)) 
                                 / (2 * LineStartToCorner * LineCornerToEnd)) * (180 / Math.PI);
            }
            public static double[] GetSidesFromVertices(params Point[] vertices)
            {
                System.Collections.Generic.List<double> sidesList = new System.Collections.Generic.List<double>();

                for (int i = 0; i < vertices.Length; i++)
                {
                    sidesList.Add(Distance(vertices[i], vertices[(i + 1) % vertices.Length]));
                }
                double[] sides = sidesList.ToArray();
                return sides;
            }
            public static double[] GetAnglesFromVertices(params Point[] vertices)
            {
                System.Collections.Generic.List<double> anglesList = new System.Collections.Generic.List<double>();
                for (int i = 0; i < vertices.Length; i++)
                {
                anglesList.Add(Angle(vertices[i], vertices[(i + 1) % vertices.Length], vertices[(i + vertices.Length - 1) % vertices.Length]));
                }
                                   double[] angles = anglesList.ToArray();
                return angles;
            }
        }
    }

