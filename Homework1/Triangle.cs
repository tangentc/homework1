﻿using System;
namespace Homework1
{
	public class Triangle : Polygon
    {
		public double side1, side2, side3;
        public Triangle(Point point1, Point point2, Point point3) 
            :base(point1, point2, point3)
        {
        }
		public override double Area()
        {
            //Area from Heron's Formula
            double s = (sides[0] + sides[1] + sides[2]) / 2;
            double squared = s * (s - sides[0]) * (s - sides[1]) * (s - sides[2]);
            return Math.Sqrt(squared);

        }

        public override double Perimeter()
        {
            return sides[0] + sides[1] + sides[2];
        }
    }
}
