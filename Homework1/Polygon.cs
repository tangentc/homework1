﻿using System;
namespace Homework1
{
    public abstract class Polygon
	{
		public Point[] vertices;
		public double[] sides;
		public double[] angles;

        protected Polygon(params Point[] points)
		{
			//Constructs polygon from vertices in cartesian plane, listed in the order in which they're connected.
			vertices = points;
            sides = Geometry.GetSidesFromVertices(vertices);
            angles = Geometry.GetAnglesFromVertices(vertices);

			if (points == null)
			{
				throw new ArgumentNullException(nameof(points));
			}

		}
		public abstract double Area();
		public abstract double Perimeter();
    }
}
