﻿using System;

namespace Homework1
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello there, what's your name?");
            string name = Console.ReadLine();
            Console.WriteLine("Hi, " + name + "! Nice to meet you.");

            Triangle tri1 = new Triangle(new Point(0,0), new Point(1,1), new Point(0,1));
			Rectangle rect1 = new Rectangle(new Point(0, 0), new Point(0,1), new Point(1,1), new Point(1,0));

            //Console.WriteLine("Please give the side lengths for a triangle. What is the length of the first side?");
            //tri1.side1 = float.Parse(Console.ReadLine());
            //Console.WriteLine("And the second side?");
            //tri1.side2 = float.Parse(Console.ReadLine());
            //Console.WriteLine("And the third side?");
            //tri1.side3 = float.Parse(Console.ReadLine());

            Console.WriteLine("The perimeter of the triangle is: " + tri1.Perimeter() + ".");
            Console.WriteLine("The area of the triangle is: " + tri1.Area() + ".");
            //Console.WriteLine("Please give the length and width of a rectangle. What is the length?");
            //rect1.length = float.Parse(Console.ReadLine());
            //Console.WriteLine("And the width?");
            //rect1.width = float.Parse(Console.ReadLine());

            Console.WriteLine("The perimeter of the rectangle is: " + rect1.Perimeter() + ".");
            Console.WriteLine("The area of the rectangle is: " + rect1.Area() + ".");

            Console.WriteLine(rect1.vertices[0].x);
            Console.WriteLine(rect1.sides[0]);
            Console.WriteLine(rect1.angles[0]);

        }
    }
}